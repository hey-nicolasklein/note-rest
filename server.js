require('dotenv').config();

const express = require('express');
const cors = require('cors');
const app = express();
const mongoose = require('mongoose');

mongoose.connect(process.env.DATABASE_URL, { useUnifiedTopology: true, useNewUrlParser: true });

const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database'))


app.use(express.json());
app.use(cors());



const notesRouter = require('./routes/notes')
app.use('/notes', notesRouter)



app.listen(process.env.PORT || 3003, () => console.log("Server started"));