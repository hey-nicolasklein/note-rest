const express = require('express');
const router = express.Router();
const Note = require('../models/notes');

//Get all notes
router.get('/', async (req, res) => {
  try{
    const notes = await Note.find()
    res.json(notes)
  }catch(err){
    res.status(500).json({message: err.message})
  }
})

//Create new note
router.post('/', async (req, res) =>{
  const note = new Note({
    title: req.body.title,
    text: req.body.text
  })

  try{
    const newNote = await note.save();
    res.status(201).send(newNote);
  }catch(err){
    res.status(400).json({message: err.message});
  }
})


//Getting one note by its id
router.get('/:id', getNote, (req, res) => {
  res.send(res.note)
})

//Deleting one note by its id
router.delete('/:id', getNote, async (req, res) => {
  try{
    await res.note.remove()
    res.status(200).json({message: 'Deleted Note'})
  }catch(err){
    res.status(500).json({message: err.message})
  }
})

//Update one note, eg. change status, title or name
router.patch('/:id', getNote, async (req, res) => {
  if(req.body.title != null){
    res.note.title = req.body.title;
  }
  if(req.body.text != null){
    res.note.text = req.body.text;
  }
  if(req.body.done != null){
    res.note.done = req.body.done;
  }
  try{
    const updatedNote = await res.note.save();
    res.json(updatedNote)
  }
  catch(err){
    err.status(400).json({message: err.message})
  }
})

//Middleware which retrieves the wanted note
async function getNote(req, res, next){
  try{
    note = await Note.findById(req.params.id);
    if(note == null){
      return res.status(404).json({message: 'Cannot find note'});
    }
  }catch(err){
      return res.status(500).json({message: err.message})
  }

  res.note = note;
  next();
}

module.exports = router 