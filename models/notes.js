const mongoose = require('mongoose')

const notesSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  text:{
    type: String,
    required: true
  },
  done:{
    type: Boolean,
    default: false
  },
  date:{
    type: Date,
    required: true,
    default: Date.now
  }
})


module.exports = mongoose.model('Notes', notesSchema)